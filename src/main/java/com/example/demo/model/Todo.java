package com.example.demo.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Todo {

    private int id;
    private String name;
    private String comment;

    public Todo(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Todo(int id, String name, String comment) {
        this(id, name);
        this.comment = comment;
    }

}
