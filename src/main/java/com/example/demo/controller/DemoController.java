package com.example.demo.controller;

import com.example.demo.model.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class DemoController {
    private List<Todo> todos;

    @Autowired
    public void setup() {
        if (this.todos == null) this.todos = new ArrayList<>();
    }

    private Todo checkTodo(int id) {
        if (id >= todos.size()) return null;
        else return todos.get(id);
    }

    @GetMapping("/todos")
    public List<Todo> getTodoList() {
        return todos;
    }

    @GetMapping("/todo/{id}")
    public Todo getTodo(@PathVariable("id") int id) {
        return checkTodo(id);
    }

    @DeleteMapping("/todo/{id}")
    public void deleteTodo(@PathVariable("id") int id) {
        if (checkTodo(id) == null) return;

        todos.remove(id);
        for (int i = 0; i < this.todos.size(); i++) {
            this.todos.get(i).setId(i);
        }
    }

    @PostMapping("/todo")
    public int addNewTodo(@RequestBody Todo todo) {
        todo.setId(this.todos.size());
        this.todos.add(todo);
        return todo.getId();
    }

    @PutMapping("/todo/{id}")
    public void updateNewTodo(@PathVariable("id") int id,
                          @RequestParam(value = "name", required = false) String name,
                          @RequestParam(value = "comment", required = false) String comment) {
        Todo todo = checkTodo(id);
        if (todo == null) return;

        if (name != null) todo.setName(name);
        if (comment != null) todo.setComment(comment);
    }

}