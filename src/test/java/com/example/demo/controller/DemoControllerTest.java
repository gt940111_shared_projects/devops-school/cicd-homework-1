package com.example.demo.controller;

import com.example.demo.model.Todo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DemoControllerTest {

    @Autowired private DemoController controller;
    @Autowired private MockMvc mvc;

    @BeforeAll
    public void setup() {
        for (int i = 0; i < 3; i++) {
            this.controller.addNewTodo(new Todo(i, "todo#" + i));
        }
    }


    @Test
    void getTodoList() {
        try {
            mvc.perform(get("/todos"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(this.controller.getTodoList().size())));
        }
        catch (Exception x) {
            System.err.println(x.getMessage());
            fail();
        }
    }

    @Test
    void getTodo() {
        try {
            mvc.perform(get("/todo/0"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.name", is("todo#0")));
        }
        catch (Exception x) {
            System.err.println(x.getMessage());
            fail();
        }
    }

    @Test
    void deleteTodo() {
        controller.addNewTodo(new Todo(999, "name#999"));
        try {
            mvc.perform(delete("/todo/2")).andExpect(status().isOk());

            List<Todo> todos = this.controller.getTodoList();
            assertTrue(todos.size() > 2);
            for (int i = 0; i < todos.size(); i++) {
                assertEquals(todos.get(i).getId(), i);
                assertNotEquals("todo#2", todos.get(i).getName());
            }
        }
        catch (Exception x) {
            System.err.println(x.getMessage());
            fail();
        }
    }

    @Test
    void addNewTodo() {
        Todo input = new Todo();
        input.setName("addNewTodo()");

        try {
            mvc.perform(post("/todo")
                            .content(new ObjectMapper().writeValueAsString(input))
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", is(this.controller.getTodoList().size()-1)));
            Todo todo = this.controller.getTodo(this.controller.getTodoList().size()-1);
            assertEquals("addNewTodo()", todo.getName());
            assertNull(todo.getComment());
        }
        catch (Exception x) {
            System.err.println(x.getMessage());
            fail();
        }
    }

    @Test
    void updateNewTodo() {
        Todo todo = this.controller.getTodo(1);
        try {
            mvc.perform(put("/todo/1?name=new name 1")).andExpect(status().isOk());
            assertEquals("new name 1", todo.getName());
            assertNull(todo.getComment());

            mvc.perform(put("/todo/1?comment=new comment")).andExpect(status().isOk());
            assertEquals("new comment", todo.getComment());
        }
        catch (Exception x) {
            System.err.println(x.getMessage());
            fail();
        }
    }
}